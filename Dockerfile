FROM redhat/ubi8-minimal:8.4

RUN microdnf update && \
    microdnf module enable python39:3.9 && \
    microdnf install python39 python39-pip && \
    curl -o oracle.rpm https://download.oracle.com/otn_software/linux/instantclient/211000/oracle-instantclient-basic-21.1.0.0.0-1.x86_64.rpm && \
    microdnf install libaio shadow-utils && \
    rpm -i oracle.rpm && \
    rm -rf /var/cache/* oracle.rpm
